### FOOD API PROJECT
# Tech stack
* [ReactJS](https://reactjs.org/)
* [Material UI](https://mui.com/)
* API Data for the project
# Main features
* Creating UI of Home page
* Finding applicable API for the project & bind it to the app
* Creating Search input
* Displaying searched data on a book page
* Creating a modal for each searched item
# Extra features
* Uploading a picture for food recognition
# Requirements
* At least 3 commits from each member of the team
* Add something which is not covered in the classes (eg. voice recognition in a search input)
* Create issues on GitHub and close them as they have been completed.
### GitHub
# How to create pull request (PR).
  1. Pull latest version of main:
      ```
      $ git checkout main
      $ git pull origin main
      $ git checkout -b <your branch name>
      ```
  2. After finishing the task:
      ```
      $ git add .
      $ git commit -m "Briefly describe what you did."
      $ git push origin <your branch name>
  3. Then go to https://github.com/AichurokT/Food-API-Project-Team1/pulls.
  4. Assign all members to code-review.
  5. Create PR to 'main' branch.
  6. Inform team in Slack.
  7. DO NOT MERGE! Wait for feedback.
  8. If your PR is returned, do not create new branch or new PR.
      Do all fixings on current branch. Then,
      ```
      $ git add .
      $ git commit -m "Made fixings"
      $ git push
      ```
  9. GO TO -> step 6.
  10. Remember, _>>>> DO NOT MERGE!!<<<<_
### How to do a code review
  1. Be familiar with the issue.
  2. See the code if it is clean, easy to read.
  3. Pull the reviewing branch to you local machine and try to run, and check if it works properly.
  4. Make comments of what you notice or demand correction(if necessary).
  5. Inform person who created PR.
  6. Be nice to people