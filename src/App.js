import React from 'react';
import axios from "axios";
import { useState, useEffect } from 'react';
import './App.css';
import Upload from './components/Upload';
import MyBook from './components/MyBook';
import Search from './components/Search';
import ModalForm from './components/ModalForm';
import { Button } from '@mui/material';

function App() {
  const [data, setData] = useState();
  const [mealValue, setMealValue] = useState();
  const [searchValue, setSearchValue] = useState('');
  const [fileToUpload, setFileToUpload] = useState('');

  const [modalType, setModalType] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = (selectedModalType) => {
    setModalType(selectedModalType);
    setIsModalOpen(true);
  }

  const handleInput = (event) => {
    setMealValue(event.target.value);
  }

  const handleSearch = () => {
    setSearchValue(mealValue);
  }

  const getSuggestion = async () => {
    const options = {
      method: 'GET',
      url: process.env.REACT_APP_RAPID_API_URL + '/recipes/mealplans/generate',
      params: { // Params values should be added by user in modal form
        timeFrame: 'day', // has just 2 options day/week
        targetCalories: '2000',
        diet: 'vegetarian', // "vegetarian", "vegan", "paleo" etc.
        exclude: 'shellfish, olives'
      },
      headers: {
        'x-rapidapi-key': process.env.REACT_APP_API_KEY,
        'x-rapidapi-host': process.env.REACT_APP_RAPID_API_HOST
      }
    };

    try {
      const resp = await axios.request(options);
      const data = resp.data;
      console.log(data)
      alert('check devtool console')
    } catch (error) {
      console.error("SUGGESTION LIST ERROR: ", error);
    }
  }

  const getMenu = async () => {
    const options = {
      method: 'GET',
      url: process.env.REACT_APP_RAPID_API_URL + '/food/menuItems/search',
      params: {
        query: searchValue,
        offset: '0',
        number: '10',
      },
      headers: {
        'x-rapidapi-key': process.env.REACT_APP_API_KEY,
        'x-rapidapi-host': process.env.REACT_APP_RAPID_API_HOST
      }
    };

    try {
      const resp = await axios.request(options);
      const data = resp.data;
      console.log('res Array: ', data);
      setData(data.menuItems);
      setMealValue('');
    } catch (error) {
      console.error("MENU LIST ERROR: ", error);
    }
  }

  const handleFileAttachment = (event) => {
    console.log('file',event.target)
    const image = event.target.files[0];
    if(image.type.includes('jpeg') || image.type.includes('jpg') || image.type.includes('png')) {
      setFileToUpload(image);
    } else {
      alert('File must be an image')
    }
  }

  const handleUpload = async () => {
    const form = new FormData();
    form.append("file", fileToUpload);

    const options = {
      method: 'POST',
      url: process.env.REACT_APP_RAPID_API_URL + '/food/images/classify',
      headers: {
        'content-type': 'multipart/form-data; boundary=---011000010111000001101001',
        'x-rapidapi-key': process.env.REACT_APP_API_KEY,
        'x-rapidapi-host': process.env.REACT_APP_RAPID_API_HOST
      },
      data: form
    };

    try {
      const resp = await axios.request(options);
      console.log('res by img',resp.data);
      setSearchValue(resp.data.category);
      setFileToUpload(null);
    } catch (error) {
      console.error('ERRRRR:', error);
    }

  }

  useEffect(() => {
    if(searchValue.length !== 0) {
      getMenu();
    }
  }, [searchValue])

  return (
    <div className="App">
      <header className='Header'>
        <h1>Discover the Right Food</h1>
        <div className='Buttons'>
          <Upload name='select image'
            handleFileAttachment={handleFileAttachment}
            handleUpload={handleUpload}
            fileToUpload={fileToUpload} />
          <Button
            color='success'
            variant='contained'
            size='small'
            onClick={openModal}  
            variant="filled" 
            >Get suggestions</Button>
          <Search
            handleInput={handleInput}
            inputValue={mealValue} 
            handleSearch={handleSearch} 
          />
        </div>
      </header>

      <main>
        <div className='Results'><MyBook dishes={data}/></div>
        <ModalForm
            isModalOpen={isModalOpen}
            setIsModalOpen={setIsModalOpen}
            modalType={modalType}
            getSuggestion={getSuggestion}
          />
      </main>
    </div>
  );
}
export default App;