import { Button, Modal, TextField } from '@mui/material';
import { Box } from '@mui/system';
import React, { useState } from 'react';
import styled from 'styled-components';

const style = {
  position: 'absolute',
  top: '10%',
  left: '50%',
  transform: 'translate(-50%,50%)',
  width: 400,
  border: '2px solid #C6E7CE',
  boxShadow: 24,
  padding: 4,
  backgroundColor: '#C6E7CE',
};

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 10px;
`;

const ModalForm = ({ isModalOpen, setIsModalOpen, getSuggestion, modalType }) => {
  const [time, setTime] = useState();
  const [calories, setCalories] = useState('');
  const [diet, setDiet] = useState('');
  const [exclusion, setExclusion] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsModalOpen(false);
    getSuggestion()
  };

  return (
    <Modal open={isModalOpen}>
      <Box sx={style}>
        <Form onSubmit={handleSubmit}>
          <TextField
            placeholder='Time'
            onChange={(e) => setTime(e.target.value)}
            value={time}
          />
          <TextField
            placeholder='Target calorie'
            onChange={(e) => setCalories(e.target.value)}
            value={calories}
          />
          <TextField
            placeholder='Preferred diet'
            onChange={(e) => setDiet(e.target.value)}
            value={diet}
          />
          <TextField
            placeholder='Exclusions'
            onChange={(e) => setExclusion(e.target.value)}
            value={exclusion}
          />
          <Button color='success' type='submit'>
            SUBMIT
          </Button>
        </Form>
      </Box>
    </Modal>
  );
};

export default ModalForm;