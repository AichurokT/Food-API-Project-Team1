import React from 'react';
import HTMLFlipBook from 'react-pageflip';
import '../components/MyBook.css';

function MyBook({dishes}) {
    return (
      <div>
        <HTMLFlipBook
        width={450} 
        height={470} 
        size="fixed"
        minWidth={250}
        maxWidth={450}
        minHeight={300}
        maxHeight={800}
        mobileScrollSupport={false}
        autoSize={true}>
          {
            dishes?.map((dish, ind) => {
              return(
                <div className="page" >
                  <h2 className='Titles'>{dish.title}</h2>
                  <img className='Images' src={dish.image} alt="" />
                  <h4 className='Servings'>{dish.servingSize}</h4>
                  <p className='page-number'>{ind + 1}</p>
                </div>
              )
            })
          }
        </HTMLFlipBook>
      </div>
    );
}

export default MyBook;