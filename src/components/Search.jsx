import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import Box from '@mui/material/Box';
import Input from '@mui/material/Input';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';

const Search = ({ handleInput, inputValue, handleSearch }) => {
    return (
        <div className='Search'>
            <Box sx={{ '& > :not(style)': { ml: 5 } }}>
                <FormControl variant="standard">
                <Input
                    type="text" 
                    size='medium'
                    onChange={handleInput} value={inputValue} placeholder='Search...'
                    id="input-with-icon-adornment"
                    startAdornment={
                <InputAdornment position="start">
                    <SearchRoundedIcon onClick={handleSearch}/>
                </InputAdornment>
            }
                />
                </FormControl>
            </Box>
        </div>
    )
};
export default Search;