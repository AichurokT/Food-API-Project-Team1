import './Upload.css'
import { Button } from '@mui/material';

const Upload = ({ name, handleUpload, handleFileAttachment, fileToUpload }) => {
    return (
      <div className='Upload'>
        <input className='Inp'
          name='select image' type="file" onChange={handleFileAttachment} fileToUpload={fileToUpload}/>
          <Button
           variant="outlined" 
           size='small' 
           color="inherit" 
           onClick={handleUpload}>
            Upload
          </Button>
      </div>
    )
};

export default Upload;